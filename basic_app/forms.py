from django import forms
from .models import UserProfileInfo
from django.contrib.auth.models import User

form_control_class = 'form-control'


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': form_control_class}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': form_control_class}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': form_control_class}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': form_control_class}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': form_control_class}))

    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name']


class UserProfileInfoForm(forms.ModelForm):
    portfolio_site = forms.CharField(widget=forms.URLInput(attrs={'class': form_control_class}))

    class Meta:
        model = UserProfileInfo
        fields = ['portfolio_site', 'profile_picture']
