from django.shortcuts import render
from .forms import UserForm, UserProfileInfoForm

from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def index(req):
    data = {'title': 'Home'}
    return render(req, 'basic_app/index.html', context=data)


def register(req):
    registered = False

    if req.method == 'POST':
        # check form validity
        user_form = UserForm(req.POST)
        user_profile_form = UserProfileInfoForm(req.POST)

        if user_form.is_valid() and user_profile_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user

            # Profile picture
            if 'profile_picture' in req.FILES:
                user_profile.profile_picture = req.FILES['profile_picture']

            user_profile.save()
            registered = True

        else:
            print('FORM INVALID')
            print(user_form.errors, user_profile_form.errors)
    else:
        user_form = UserForm()
        user_profile_form = UserProfileInfoForm()

    data = {'title': 'Register', 'user_form': user_form,
            'user_profile_form': user_profile_form, 'registered': registered}
    return render(req, 'basic_app/registration.html', context=data)


def user_login(req):
    if req.method == 'POST':
        username = req.POST.get('username')
        password = req.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(req, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                # User is not active anymore
                return HttpResponse('ACCOUNT NOT ACTIVE')
        else:
            # bad credentials
            print('Failed login attempt')
            print('Username %s, Password %s' % (username, password))
            return HttpResponse('BAD CREDENTIALS')

    else:
        return render(req, 'basic_app/login.html')


@login_required
def user_logout(req):
    logout(req)
    return HttpResponseRedirect(reverse('index'))


@login_required
def special(req):
    return HttpResponse('Something special')
